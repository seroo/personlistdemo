package personlist.demo;

public class Customer {
	
	
	private String mail;
	private String phoneNumber;
	
	Customer(String name, String mail, String phoneNumber) {
		this.name = name;
		this.mail = mail;
		this.phoneNumber = phoneNumber;
	}
	
	private String name;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	
	
	@Override
	public String toString() {
		String s = "Customer Name: " + getName() + "\nEmail: " + getMail() + "\nPhone Number: " + getPhoneNumber();
		return s;
	}
	
	

	
	
}
